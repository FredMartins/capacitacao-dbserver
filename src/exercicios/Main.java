package exercicios;

import java.util.Scanner;

import exercicios.ex1.Ex1;
import exercicios.ex10.Ex10;
import exercicios.ex11.Ex11;
import exercicios.ex12.Ex12;
import exercicios.ex13.Ex13;
import exercicios.ex14.Ex14;
import exercicios.ex15.Ex15;
import exercicios.ex2.Ex2;
import exercicios.ex3.Ex3;
import exercicios.ex4.Ex4;
import exercicios.ex5.Ex5;
import exercicios.ex6.Ex6;
import exercicios.ex7.Ex7;
import exercicios.ex8.Ex8;
import exercicios.ex9.Ex9;

public class Main {
	public static void main(String [] args) {
		int opcao = 0;
		Scanner sc = new Scanner(System.in);
		do {
			System.out.println("\nDigite um valor para iniciar o exercicio");
			for(int i = 1; i <=20;i++) {
				System.out.println(i+ " -> Exerc�cio "+i);
			}
			System.out.println("0 -> Sair");
			opcao = sc.nextInt();
			switch(opcao) {
			case 1: 
				Ex1 ex1 = new Ex1();
				ex1.verificaIdade();
				break;
			case 2:
				Ex2 ex2 = new Ex2();
				ex2.compras();
				break;
			case 3:
				Ex3 ex3 = new Ex3();
				ex3.jokenpo();
				break;
			case 4:
				Ex4 ex4 = new Ex4();
				ex4.calculadora();
				break;
			case 5:
				Ex5 ex5 = new Ex5();
				ex5.tabuada();
				break;
			case 6:
				Ex6 ex6 = new Ex6();
				ex6.maiorMenor();
				break;
			case 7:
				Ex7 ex7 = new Ex7();
				ex7.fibonacci();
				break;
			case 8:
				Ex8 ex8 = new Ex8();
				ex8.not10();
				break;
			case 9:
				Ex9 ex9 = new Ex9();
				ex9.fibonacci();
				break;
			case 10:
				Ex10 ex10 = new Ex10();
				ex10.somaDiag();
				break;
			case 11:
				Ex11 ex11 = new Ex11();
				ex11.ordenaVetor();
				break;
			case 12:
				Ex12 ex12 = new Ex12();
				ex12.vetores();
				break;
			case 13:
				Ex13 ex13 = new Ex13();
				ex13.notaFinalAval();
				break;
			case 14:
				Ex14 ex14 = new Ex14();
				ex14.transformSeg();
				break;
			case 15:
				Ex15 ex15 = new Ex15();
				ex15.calculadora();
				break;
			case 0:
				break;
			}
		}while(opcao != 0);
		sc.close();
		System.out.println("At� logo!");
	}
}
