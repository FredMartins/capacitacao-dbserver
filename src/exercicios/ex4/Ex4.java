package exercicios.ex4;

import java.util.Scanner;

public class Ex4 {
	private double calcula(double num1,double num2, String op) {
		double resp = 0;
		switch(op) {
		case "+":
			resp = num1+num2;
			break;
		case "-":
			resp = num1-num2;
			break;
		case "/":
			resp = num1/num2;
			break;
		case "*":
			resp = num1*num2;
			break;
		}
		return resp;
	}
	
	public void calculadora() {
		int opcao = 0;
		Scanner in = new Scanner(System.in);
		System.out.println("Informe o primeiro valor: ");
		double num1 = in.nextDouble();
		System.out.println("\nInforme o segundo valor: ");
		double num2 = in.nextDouble();
		System.out.println("\nEscolha uma opera��o: ");
		System.out.println("1 -> Soma ");
		System.out.println("2 -> Subtra��o ");
		System.out.println("3 -> Divis�o ");
		System.out.println("4 -> Multiplica��o ");
		opcao = in.nextInt();
		String op = "";
		switch(opcao) {
		case 1:
			op = "+";
			break;
		case 2:
			op = "-";
			break;
		case 3:
			op = "/";
			break;
		case 4:
			op = "*";
			break;
		}
		System.out.println("Resultado:\n"+num1+""+op+""+num2+" = "+calcula(num1,num2,op));
	}
}
