package exercicios.ex5;

import java.util.Scanner;

public class Ex5 {
	public void tabuada() {
		Scanner in = new Scanner(System.in);
		System.out.println("\nTabuada!!");
		System.out.println("Digite um n�mero: ");
		int val = in.nextInt();
		System.out.println("\nResultados: ");
		for (int i = 1; i<=10; i++) {
			System.out.println(val+"x"+i+"="+(val*i));
		}
	}
}
