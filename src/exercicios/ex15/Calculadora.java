package exercicios.ex15;

public class Calculadora {
	public static double soma(double n1, double n2) {
		return n1+n2;
	}
	public static double subtracao(double n1, double n2) {
		return n1-n2;
	}
	public static double multiplicacao(double n1, double n2) {
		return n1*n2;
	}
	public static double divisao(double dividendo, double divisor) {
		return dividendo/divisor;
	}
	public static double potencia(double base, double expoente) {
		return Math.pow(base, expoente);
	}
}
