package exercicios.ex9;

import java.util.Scanner;

public class Ex9 {
	
	public void fibonacci() {
		Scanner in = new Scanner(System.in);
		System.out.println("\nFibonacci!");
		System.out.println("Digite um valor n�o negativo: ");
		int n;
		do {
			n = in.nextInt();
			if(n <= 0) {
				System.out.println("Valor deve ser positivo! Tente novamente: ");
			}
		}while(n <= 0);
		int f0 = 0;
		int f1 = 1;
		int soma = 0;
		System.out.print(f0+" "+f1+" ");
		soma = f0+f1;
		while(soma <= n) {
			System.out.print(soma+" ");
			f0 = f1;
			f1 = soma;
			soma = f0+f1;
		}
		System.out.println(" ");
		/*
		int[]vals = fibVals(n);
		System.out.println("\nResultado:");
		for(int i = 0; i< vals.length;i++) {
			System.out.print(vals[i]+" ");
		}
		*/
	}
}
