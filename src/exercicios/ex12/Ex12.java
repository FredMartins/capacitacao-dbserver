package exercicios.ex12;

import java.util.Random;

public class Ex12 {
	private int[] valores(int n) {
		Random r = new Random();
		int[] vals = new int[n];
		for(int i = 0; i<vals.length;i++) {
			vals[i] = r.nextInt(500);
		}
		return vals;
	}
	
	private void printArray(int[]v) {
		for(int i = 0; i<v.length; i++) {
			System.out.print(v[i]+" ");
		}
		System.out.println("");
	}
	
	private int[] ordena(int[] vals) {
		int aux = 0;
		int []vaux = vals;
		for(int i = 0; i<vaux.length; i++) {
			for(int j = i; j<vaux.length; j++) {
				if(vaux[j] <= vaux[i]) {
					aux = vaux[i];
					vaux[i] = vaux[j];
					vaux[j] = aux;
				}
			}
		}
		return vaux;
	}
	
	public void vetores() {
		int[] vet1 = valores(50);
		int[] vet2 = valores(50);
		int[] result = new int[100];
		
		System.out.print("\nVetor 1 antes da ordena��o: ");
		printArray(vet1);
		System.out.print("\nVetor 2 antes da ordena��o: ");
		printArray(vet2);
		int[] vet1Aux = ordena(vet1);
		vet1 = vet1Aux;
		int[] vet2Aux = ordena(vet2);
		vet2 = vet2Aux;
		System.out.print("\nVetor 1 depois da ordena��o: ");
		printArray(vet1);
		System.out.print("\nVetor 2 depois da ordena��o: ");
		printArray(vet2);
		int j = 0;
		for(int i = 0; i<vet1.length; i++) {
			result[i] = vet1[i];
			j = i;
		}
		int index = 0;
		for(int k = j+1; k < result.length; k++) {
			result[k] = vet2[index];
			index += 1;
		}
		System.out.print("\nVetor resposta antes da ordena��o: ");
		printArray(result);
		int[] resultAux = ordena(result);
		result = resultAux;
		System.out.print("\nVetor resposta antes da ordena��o: ");
		printArray(result);
	}
}
