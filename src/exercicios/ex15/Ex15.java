package exercicios.ex15;

import java.util.Scanner;

public class Ex15 {
	public void calculadora() {
		int opcao = 0;
		Scanner in = new Scanner(System.in);
		System.out.println("Informe o primeiro valor: ");
		double num1 = in.nextDouble();
		System.out.println("\nInforme o segundo valor: ");
		double num2 = in.nextDouble();
		double resp = 0;
		System.out.println("\nEscolha uma operação: ");
		System.out.println("1 -> Soma ");
		System.out.println("2 -> Subtração ");
		System.out.println("3 -> Divisão ");
		System.out.println("4 -> Multiplicação ");
		System.out.println("5 -> Potenciação ");
		opcao = in.nextInt();
		String op ="";
		switch(opcao) {
		case 1:
			op = "+";
			resp = Calculadora.soma(num1, num2);
			break;
		case 2:
			op = "-";
			resp = Calculadora.subtracao(num1, num2);
			break;
		case 3:
			op = "/";
			resp = Calculadora.divisao(num1, num2);
			break;
		case 4:
			op = "*";
			resp = Calculadora.multiplicacao(num1, num2);
			break;
		case 5:
			op = "^";
			resp = Calculadora.potencia(num1, num2);
			break;
		}
		System.out.println("Resultado:\n"+num1+""+op+""+num2+" = "+resp);
	}
}
