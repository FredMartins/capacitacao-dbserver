package exercicios.ex13;

import java.util.Scanner;

public class Ex13 {
	public double media(double n1, double n2, double n3) {
		return (n1+n2+n3)/3;
	}
	
	public void resultado(double nota) {
		if(nota>=6) {
			System.out.println("Aprovado!");
		}else if(nota >=4 && nota<6){
			System.out.println("Verifica��o Suplementar!");
		}else {
			System.out.println("Reprovado!");
		}
	}
	
	public void notaFinalAval() {
		Scanner in = new Scanner(System.in);
		double n1,n2,n3;
		System.out.println("Informe a primeira nota: ");
		n1 = in.nextDouble();
		System.out.println("Informe a segunda nota: ");
		n2 = in.nextDouble();
		System.out.println("Informe a terceira nota: ");
		n3 = in.nextDouble();
		double mediaNotas = media(n1,n2,n3);
		System.out.printf("M�dia final: %.1f\n",mediaNotas);
		System.out.println("Resultado: ");
		resultado(mediaNotas);
	}
}
