package exercicios.ex10;

import java.util.Random;
import java.util.Scanner;

public class Ex10 {
	private int[][] matriz(int n){
		int[][] m = new int[n][n];
		Random r = new Random();
		for(int i = 0; i<m.length;i++) {
			for(int j = 0; j<m[0].length ;j++) {
				m[i][j] = r.nextInt(100);
			}
		}
		return m;
	}
	
	public void somaDiag() {
		Scanner in = new Scanner(System.in);
		System.out.println("Informe o tamanho da matriz:");
		int n = in.nextInt();
		int [][] mat = matriz(n);
		int soma = 0;
		System.out.println("\nMatriz:");
		for(int k = 0; k<mat.length; k++) {
			for(int l = 0; l<mat[0].length; l++) {
				System.out.print(mat[k][l]+" ");
			}
			System.out.println("");
		}
		
		System.out.print("\nValores da diagonal principal: ");
		for(int i = 0; i< mat.length; i++) {
			for(int j = 0; j < mat[0].length; j++) {
				if(i == j) {
					System.out.print(mat[i][j]+" ");
					soma += mat[i][j];
				}
			}
		}
		System.out.println("\nResultado da soma da diagonal: "+soma);		
	}
}
