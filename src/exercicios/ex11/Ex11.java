package exercicios.ex11;

import java.util.Random;

public class Ex11 {
	private int[] valores() {
		Random r = new Random();
		int[] vals = new int[100];
		for(int i = 0; i<vals.length;i++) {
			vals[i] = r.nextInt(1000);
		}
		return vals;
	}
	
	private void printArray(int[]v) {
		for(int i = 0; i<v.length; i++) {
			System.out.print(v[i]+" ");
		}
		System.out.println("");
	}
	
	public void ordenaVetor() {
		int[] vals = valores();
		int aux = 0;
		System.out.print("\nValores antes da ordena��o: ");
		printArray(vals);
		for(int i = 0; i<vals.length; i++) {
			for(int j = i; j<vals.length; j++) {
				if(vals[j] <= vals[i]) {
					aux = vals[i];
					vals[i] = vals[j];
					vals[j] = aux;
				}
			}
		}
		System.out.print("\nValores depois da ordena��o: ");
		printArray(vals);
	}
}
