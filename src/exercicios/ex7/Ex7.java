package exercicios.ex7;

import java.util.Scanner;

public class Ex7 {
	
	private int[] fibVals(int n) {
		int[] vals = new int[n+1];
		vals[0] = 0;
		vals[1] = 1;
		for(int i = 2; i <vals.length; i++) {
			vals[i] = vals[i-2] + vals[i-1];
		}
		return vals;
	}
	
	public void fibonacci() {
		Scanner in = new Scanner(System.in);
		System.out.println("\nFibonacci!");
		System.out.println("Digite um valor n�o negativo: ");
		int n;
		do {
			n = in.nextInt();
			if(n <= 0) {
				System.out.println("Valor deve ser positivo! Tente novamente: ");
			}
		}while(n <= 0);
		int[]vals = fibVals(n);
		System.out.println("\nResultado:");
		for(int i = 0; i< vals.length;i++) {
			System.out.print(vals[i]+" ");
		}
	}
}
