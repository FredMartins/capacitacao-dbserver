package exercicios.ex3;

import java.util.Random;
import java.util.Scanner;

public class Ex3 {
	private String msgFinal(String jogador, String maquina) {
		String msg = "";
		if(jogador.equalsIgnoreCase("tesoura")) {
			if(maquina.equalsIgnoreCase("pedra")) {
				msg = "Voc� PERDEU!";
			}else if(maquina.equalsIgnoreCase("papel")) {
				msg = "Voc� VENCEU!";
			}else {
				msg = "EMPATE!";
			}
		}
		if(jogador.equalsIgnoreCase("papel")) {
			if(maquina.equalsIgnoreCase("tesoura")) {
				msg = "Voc� PERDEU!";
			}else if(maquina.equalsIgnoreCase("pedra")) {
				msg = "Voc� VENCEU!";
			}else {
				msg = "EMPATE!";
			}
		}
		if(jogador.equalsIgnoreCase("pedra")) {
			if(maquina.equalsIgnoreCase("papel")) {
				msg = "Voc� PERDEU!";
			}else if(maquina.equalsIgnoreCase("tesoura")) {
				msg = "Voc� VENCEU!";
			}else {
				msg = "EMPATE!";
			}
		}
		return msg;
	}
	
	public void jokenpo() {
		Scanner in = new Scanner(System.in);
		String [] maquina = {"pedra", "papel", "tesoura"};
		Random r = new Random();
		int pos = r.nextInt(maquina.length);
		String respMaquina = maquina[pos];
		System.out.print("Jokenpo!\nPedra, papel ou tesoura!\nInforme a sua jogada: ");
		String respJogador = in.nextLine();
		System.out.print("\nJogada da MAQUINA: "+respMaquina+"\nSua jogada: "+respJogador+"\n"+msgFinal(respJogador,respMaquina)+"\n");
		System.out.println("Obrigado por jogar!");
	}
}
