package exercicios.ex14;

import java.util.Scanner;

public class Ex14 {
	
	private int leitura() {
		Scanner in = new Scanner(System.in);
		System.out.println("Digite um tempo em segundos: ");
		int seg = in.nextInt();
		return seg;
	}
	
	private void escrita(int h, int min, int seg) {
		System.out.println("Resultado: "+h+"h"+min+"min"+seg+"seg");
	}
	
	private int[] segToHour(int seg) {
		int[] aux = new int[2];
		aux[0] = seg/3600;
		aux[1] = seg%3600;
		return aux;
	}
	
	private int[] segToMin(int seg) {
		int[] aux = new int[2];
		aux[0] = seg/60;
		aux[1] = seg%60;
		return aux;
	}
	
	public void transformSeg() {
		int seg = leitura();
		int[] hora = segToHour(seg);
		seg = hora[1];
		int[] min = segToMin(seg);
		seg = min[1];
		escrita(hora[0],min[0],seg);
	}
}
