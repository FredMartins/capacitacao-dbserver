package exercicios.ex6;

import java.util.Random;

public class Ex6 {
	
	private int[] valores() {
		Random r = new Random();
		int[]aux = new int[10];
		for(int i=0;i<aux.length;i++) {
			aux[i] = r.nextInt(100);
		}
		return aux;
	}
	
	public void maiorMenor() {
		int maior = Integer.MIN_VALUE;
		int menor = Integer.MAX_VALUE;
		int[] valores = valores();
		System.out.print("\nValores gerados: ");
		for (int i = 0; i<valores.length;i++) {
			System.out.print(valores[i]+" ");
		}
		System.out.println("\n");
		for(int j = 0;j<valores.length;j++) {
			if(valores[j] > maior) {
				maior = valores[j];
			}
			if(valores[j] < menor) {
				menor = valores[j];
			}
		}
		System.out.println("Maior valor: "+maior);
		System.out.println("Menor valor: "+menor);
	}
}
